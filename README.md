# Setup Instructions

* Checkout *singleFrameActorClassical* branch and adapt `/src/main/resources/application.conf` accordingly.
* Branch *multipleFrameActorClassical* requires video file distribution, is slower than sending actual frames, and is not up to date with *singleFrameActorClassical*.
* Branch *singleFrameActorArtery* is broken due to too large messages  
  (https://github.com/akka/akka/issues/22257)

# Dynamic Joining of Worker Nodes Using Clustering

Akka's Remoting capabilities can be expanded by Akka Clustering. It provides a way to form a more elastic system that can adapt to the workload at runtime by adding additional cluster nodes, putting them to sleep or removing them completely.

Regarding the designs that only leverage the Remoting features, one worth mentioning implication is that addresses to remote actors in the 'tracker / detector' systems have to be known by the 'main' system beforehand and started first in order to process video frames. Theoretically, it is possible to avoid this and start another group of `DetectionActor` instances that immediately pick up work from an already running video analysis but would require a significant additional coding effort on application level.

The `akka-cluster` module builds on top of that allowing more sophisticated designs that scale and adapt more accordingly to the system's workload. For instance, a system that is already delegating work to two detecting systems can instantly involve another system that has been started in the meantime---without stopping, reconfiguring and restarting all systems. However, it should be mentioned that the 'asynchronous actor integration' pattern is not suited for Clustering because its graph is constructed in the 'main' system and would have to be reconstructed whenever a new node joins the cluster. Therefore, Clustering is better suited for the work-pulling pattern where the amount of parallel running `DetectionActor` instances can scale dynamically and instantly take over work.

Regarding the use case of tracking specific vehicles by a single user in a known system environment, a more dynamic and flexible clustered architecture is less relevant for this master's thesis but is definitely worth to be investigated in more detail.